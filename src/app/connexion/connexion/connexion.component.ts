import { Component, OnInit } from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {AuthentificationService} from "../../service/authentification.service";

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  login    : string ;
  password : string ;

  constructor(public authService: AuthentificationService,
              private toastr : ToastrService,
           private router : Router) { }

  ngOnInit(): void {
      this.authService.clearData();
  }


  loginUser(les_valeurs) {
console.log(les_valeurs);
    this.authService.loginUser(les_valeurs).subscribe(
        data=>{
          console.log(data);
          if(data.code==200){
              this.router.navigate(['/dashboard'])
              this.toastr.success('Successfully Signed Up!');
              this.authService.setLocalInfoUtilisateur(data);
              this.authService.setLocalUserProfile(data.data.token);
          }else {
              this.toastr.error('Votre compte est desactivé');
          }

        },
        err=>{
          console.log(err);
          //this.toastr.error('Access denied');
        })
     }

}
