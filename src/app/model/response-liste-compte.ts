import {RetourConnexion} from "./retour-connexion";
import {Compte} from "./compte";

export class ResponseListeCompte {

    public code:number ;
    public message: string;
    public data: Compte[];

    constructor() {

    }
}
