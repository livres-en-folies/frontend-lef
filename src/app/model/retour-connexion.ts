import {Utilisateur} from "./utilisateur";

export class RetourConnexion {
    public utilisateur : Utilisateur;
    public token : string;
}
