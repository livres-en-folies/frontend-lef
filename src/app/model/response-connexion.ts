import {RetourConnexion} from "./retour-connexion";

export class ResponseConnexion {
    public code:number ;
    public message: string;
    public data: RetourConnexion;

}
