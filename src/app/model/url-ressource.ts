export class UrlRessource {
    public static ROOT_RESSOURCE :string="http://localhost:9090/bank/webresources/";
    public static LOGIN_RESSOURCE :string=UrlRessource.ROOT_RESSOURCE+'users/connexion';
    public static LISTE_COMPTE_RESSOURCE :string=UrlRessource.ROOT_RESSOURCE+'compte';
    public static LISTE_RESUME_DASHBOARD :string=UrlRessource.ROOT_RESSOURCE+'compte/resume';
    public static LISTE_UTILISATEUR_RESSOURCE :string=UrlRessource.ROOT_RESSOURCE+'users';
    public static ADD_COMPTE_RESSOURSE :string=UrlRessource.ROOT_RESSOURCE+'compte';
}
