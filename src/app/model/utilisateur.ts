import {Role} from "./role";
import {Adress} from "./adress";

export class Utilisateur {

    public id :number ;
    public firstName :string ;
    public lastName :string ;
    public login :string ;
    public password :string ;
    public role :Role;

    public adress :Adress;

    constructor() {
        this.adress= new Adress();
    }

}
