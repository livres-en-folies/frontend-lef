import {Compte} from "./compte";

export class ResponseOneCompte {

    public code:number ;
    public message: string;
    public data: Compte;

    constructor() {

    }
}
