import {Utilisateur} from "./utilisateur";
import {Operation} from "./operation";

export class Compte {
    public id:number;
    public numero:string;
    public montant:number;
    public owner: Utilisateur ;
    public operations: Operation[];

    constructor() {
        this.owner= new Utilisateur();
    }
}
