import {Operation} from "./operation";

export class ResumeDashboard {

    public  nbCompte:number;
    public nbOperation:number;
    public  nbUtilisateur:number;
    public  nbMontant:number;
    public lops:Operation[];

}
