import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import {ResumeDashboard} from "../model/resume-dashboard";
import {AuthentificationService} from "../service/authentification.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    resumeDash : ResumeDashboard= new ResumeDashboard()


  constructor(public authService: AuthentificationService) { }

  ngOnInit() {
        this.refreshDataTable();
  }

    refreshDataTable(){
        this.authService.listResumeDashboard().
        subscribe( res => {
                console.log(res);
                this.resumeDash = res
            },
            err => {
                // console.log(err)
            }

        );
    }

}
