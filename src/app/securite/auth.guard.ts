import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthentificationService} from "../service/authentification.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private userAuthService: AuthentificationService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //alert("GOD my life");
    if (this.userAuthService.getLocalStorageUser()) {
      // logged in so return true
     // alert("GOD my life");
      return true
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/connexion']);
    return false;
  }
  
}
