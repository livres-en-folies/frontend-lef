import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthentificationService} from "../service/authentification.service";
import {Compte} from "../model/compte";

@Component({
  selector: 'app-compte-detail',
  templateUrl: './compte-detail.component.html',
  styleUrls: ['./compte-detail.component.css']
})
export class CompteDetailComponent implements OnInit {
  idCompte:number;
  compte :Compte= new Compte();
  constructor(public authService: AuthentificationService, private route :ActivatedRoute,
               private router :Router ) { }

  ngOnInit(): void {
    this.idCompte= +this.route.snapshot.paramMap.get('id');
   this.refreshDataTable();
  }

  refreshDataTable(){
    this.authService.oneCompte(this.idCompte).
    subscribe( res => { this.compte = res.data },
        err => {
          // console.log(err)
        }
    );

  }

}


