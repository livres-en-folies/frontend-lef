import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
/*import {JwtHelper} from 'angular2-jwt';*/
import {Utilisateur} from "../model/utilisateur";
import {switchMap} from 'rxjs/operators';
import {UrlRessource} from "../model/url-ressource";
import {ResponseConnexion} from "../model/response-connexion";
import {ResponseListeCompte} from "../model/response-liste-compte";
import {ResumeDashboard} from "../model/resume-dashboard";
import {ResponseOneCompte} from "../model/response-one-compte";


@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {


  /*user : Observable<firebase.User>;*/
  userData   : any;
  isLoggedIn = false;

  constructor(private router : Router,
              private toastr : ToastrService,
              private httpClient: HttpClient) {
  }

  /*
 *  getLocalStorageUser function is used to get local user profile data.
 */
  getLocalStorageUser(){
    this.userData = JSON.parse(sessionStorage.getItem("userProfile"));
    if(this.userData) {
      this.isLoggedIn = true;
      return true;
    } else {
      this.isLoggedIn = false;
      return false;
    }
  }

  /*
   * loginUser fuction used to login
   */
  loginUser(value) : Observable<ResponseConnexion> {
    // console.log(value);
    sessionStorage.removeItem("userProfile");
    sessionStorage.removeItem("infoUtilisateur");

    return this.httpClient.post<ResponseConnexion>(UrlRessource.LOGIN_RESSOURCE, value);

    /*return this.httpClient.post<any>(UrlRessource.LOGIN_RESSOURCE, value, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response'
    }).pipe(

        switchMap(data => {
          this.setLocalUserProfile(data.headers.get('Authorization'))
          let jwtHelper = new JwtHelper();
          let email =jwtHelper.decodeToken(data.headers.get('Authorization')).sub;
          return this.httpClient.get<Utilisateur>(UrlRessource.UTILISATEUR_RESSOURCE+'/'+email);
        })
    )*/
  }

  /*
 * utilisateur information Utilisateur
 */
 /* utilisateur(login :String): Observable<Utilisateur>{
    return  this.httpClient.get<Utilisateur>(UrlRessource.UTILISATEUR_RESSOURCE+'/'+login);
  }*/

  /*
 * logOut function is used to sign out
 */
  logOut() {
    this.clearData();
    this.isLoggedIn = false;
    this.toastr.success("Successfully logged out!");
    this.router.navigate(['/session/login']);
  }

  clearData(){
    sessionStorage.removeItem("userProfile");
    sessionStorage.removeItem("infoUtilisateur");
  }

  /*
 * setLocalUserProfile function is used to set local user profile data.
 */
  setLocalUserProfile(value){
    sessionStorage.setItem('userProfile', JSON.stringify(value));
    this.getLocalStorageUser();
    this.isLoggedIn = true;
  }

  /*
* setLocalInfoUtilisateur function is used to set local user profile data.
*/
  setLocalInfoUtilisateur(value){
    sessionStorage.setItem('infoUtilisateur', JSON.stringify(value));
  }

  /*
* getLocalInfoUtilisateur function is used to set local user profile data.
*/
  getLocalInfoUtilisateur(){
    return  JSON.parse(sessionStorage.getItem('infoUtilisateur'));
  }


  getToken(){
    /* return 'sjjss';*/
    let data = sessionStorage.getItem("userProfile");
    // console.log(data);
    if(data!=null){
      data =JSON.parse(data);
    }
    return data;
  }

  getRoleUser(){
   /* let token  = JSON.parse(sessionStorage.getItem("userProfile"));
    let jwtHelper = new JwtHelper();
    let roles : Array<any>;
    roles=jwtHelper.decodeToken(token).roles;
    for (let r of roles){
      return r.authority;
    }*/
    return '';
  }

  public listCompte():Observable<ResponseListeCompte>{
    return this.httpClient.get<ResponseListeCompte>(UrlRessource.LISTE_COMPTE_RESSOURCE);
  }


  public listUtilisateur():Observable<Utilisateur[]>{
    return this.httpClient.get<Utilisateur[]>(UrlRessource.LISTE_UTILISATEUR_RESSOURCE);
  }

  public listResumeDashboard():Observable<ResumeDashboard>{
    return this.httpClient.get<ResumeDashboard>(UrlRessource.LISTE_RESUME_DASHBOARD);
  }

  public creationCompte(value):Observable<any>{
    return this.httpClient.post<ResponseConnexion>(UrlRessource.ADD_COMPTE_RESSOURSE, value);
  }

  public oneCompte(id:number):Observable<ResponseOneCompte>{
    return this.httpClient.get<ResponseOneCompte>(UrlRessource.LISTE_COMPTE_RESSOURCE+"/"+id);
  }

}
