import { Component, OnInit } from '@angular/core';
import {Compte} from "../model/compte";
import {Utilisateur} from "../model/utilisateur";
import {AuthentificationService} from "../service/authentification.service";

@Component({
  selector: 'app-utilisateur-list',
  templateUrl: './utilisateur-list.component.html',
  styleUrls: ['./utilisateur-list.component.css']
})
export class UtilisateurListComponent implements OnInit {

  utilisateurList : Array<Utilisateur>= new Array<Utilisateur>();
  route_utilisateur_detail :string
  constructor(public authService: AuthentificationService) { }

  ngOnInit(): void {
    this.route_utilisateur_detail="/utilisateur-detail";
    this.refreshDataTable();
  }

  refreshDataTable(){
    this.authService.listUtilisateur().
    subscribe( res => {
          console.log(res);
          this.utilisateurList = res
        },
        err => {
          // console.log(err)
        }

    );
  }

}
