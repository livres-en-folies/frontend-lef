import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {CompteListComponent} from "../../compte-list/compte-list.component";
import {CompteDetailComponent} from "../../compte-detail/compte-detail.component";
import {UtilisateurListComponent} from "../../utilisateur-list/utilisateur-list.component";
import {UtilisateurDetailComponent} from "../../utilisateur-detail/utilisateur-detail.component";
import {AuthGuard} from "../../securite/auth.guard";



export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent, canActivate:[AuthGuard] },
    { path: 'compte-list',        component: CompteListComponent },
    { path: 'compte-detail/:id',        component: CompteDetailComponent },
    { path: 'utilisateur-list',        component: UtilisateurListComponent },
    { path: 'utilisateur-detail',        component: UtilisateurDetailComponent },

];
