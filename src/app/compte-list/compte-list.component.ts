import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from "../service/authentification.service";
import {Compte} from "../model/compte";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Utilisateur} from "../model/utilisateur";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'app-compte-list',
  templateUrl: './compte-list.component.html',
  styleUrls: ['./compte-list.component.css']
})
export class CompteListComponent implements OnInit {

  compteList : Array<Compte>;
  route_compte_detail :string;

  constructor(public authService: AuthentificationService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.route_compte_detail="/compte-detail/";
    this.refreshDataTable();
  }
  openDialog() {
    const dialogRef = this.dialog.open(DialogContentFormDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  refreshDataTable(){
    this.authService.listCompte().
    subscribe( res => {
             console.log(res);
            this.compteList = res.data
        },
        err => {
          // console.log(err)
        }

    );
  }

}

@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: './dialog-content-form-dialog.html',
})
export class DialogContentFormDialog {

  compte :Compte=new Compte();

  constructor(public authService: AuthentificationService,
              public dialogRef : MatDialogRef<DialogContentFormDialog>) {

  }

  ngOnInit(): void {

  }

  ajouterCompte(datav){
    this.compte.owner.id=0
    console.log(this.compte);
    this.authService.creationCompte(this.compte).
    subscribe( res => {
          console.log(res);
          this.dialogRef.close();
        },
        err => {
          console.log(err);
          this.dialogRef.close();
        }

    );

  }

}
